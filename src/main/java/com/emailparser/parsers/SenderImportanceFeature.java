package com.emailparser.parsers;

import com.emailparser.actor.ImmutableEmail;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class SenderImportanceFeature implements FeatureExtractor {

    Set<String> boardOfDirector = ImmutableSet.of("kenneth.lay@enron.com",
                                                  "jeffrey.killing@enron.com",
                                                  "andrew.fastow@enron.com",
                                                  "rebecca.mark-jusbasche@enron.com");

    @Override
    public ImmutableMap<String, Object> extract(ImmutableEmail email) {
        if(boardOfDirector.contains(email.getSender())){
            return ImmutableMap.of("sender-importance", 10.0);
        }
        return ImmutableMap.of("sender-importance", 5.0);
    }
}
