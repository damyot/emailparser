package com.emailparser;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.emailparser.actor.*;
import com.emailparser.actor.CoarseEmailParser.RawEmail;
import com.emailparser.parsers.*;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Place where all the actor interleaving is done.
 */
public class ActorBasedEmailPipeline implements EmailPipeline{
    private final ActorRef simpleEmailParser;
    private final ActorRef artifactExtraction;
    private final ActorRef featureExtractor;
    private final ActorRef parsedEmailPersistor;
    private final List<ActorRef> secondaryFeatureExtractor;

    public ActorBasedEmailPipeline(ActorSystem system) {
        List<ArtifactExtractor> artifactExtractors = ImmutableList.of(new TickerExtractor());
        List<FeatureExtractor> featureExtractors = ImmutableList.of(new SenderImportanceFeature());

        this.parsedEmailPersistor = system.actorOf(ParsedEmailPersistor.props());

        this.secondaryFeatureExtractor = ImmutableList.of(
                system.actorOf(SecondaryFeatureExtraction.props(new AverageBodyLength(), parsedEmailPersistor))
        );

        this.featureExtractor = system.actorOf(FeatureExtraction.props(featureExtractors, parsedEmailPersistor, secondaryFeatureExtractor));
        this.artifactExtraction = system.actorOf(ArtifactExtraction.props(artifactExtractors, featureExtractor));
        this.simpleEmailParser = system.actorOf(CoarseEmailParser.props(artifactExtraction));
    }

    @Override
    public void process(String canonicalName, String emailFullContent) {
        RawEmail payload = new RawEmail(emailFullContent, canonicalName);
        simpleEmailParser.tell(payload, ActorRef.noSender());
    }
}
