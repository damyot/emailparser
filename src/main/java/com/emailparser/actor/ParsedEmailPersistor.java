package com.emailparser.actor;

import akka.actor.AbstractActor;
import akka.actor.Props;

public class ParsedEmailPersistor extends AbstractActor {

    static public Props props() {// can but the credentials here
        return Props.create(ParsedEmailPersistor.class, ParsedEmailPersistor::new);
    }

    private void saveEmailInDb(ImmutableEmail email) {
        // nothing saves more than logging to stdout.
        System.out.println(email);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ImmutableEmail.class, this::saveEmailInDb)
                .build();
    }


}
