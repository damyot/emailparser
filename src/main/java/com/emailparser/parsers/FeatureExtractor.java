package com.emailparser.parsers;

import com.emailparser.actor.ImmutableEmail;
import com.google.common.collect.ImmutableMap;

public interface FeatureExtractor {
    /**
     * Extract features from the email.  You have access to all
     * the email and its features.
     * @param email,
     * @return a feature
     */
    ImmutableMap<String, Object> extract(ImmutableEmail email);
}
