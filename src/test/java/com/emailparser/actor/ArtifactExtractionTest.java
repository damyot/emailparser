package com.emailparser.actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import com.emailparser.parsers.ArtifactExtractor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import scala.Immutable;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class ArtifactExtractionTest {

    ActorSystem system;

    @Before
    public void setup() {
        system = ActorSystem.create();
    }

    @After
    public void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    static class MockArtifactExtractor implements ArtifactExtractor{

        @Override
        public ImmutableMap<String, Object> extract(ImmutableEmail email) {
            System.out.println("BOOM@");
            return ImmutableMap.of("boom", 90);
        }
    }

    @Test
    public void createReceive() throws IOException {
        final TestKit testProbe = new TestKit(system);
        MockArtifactExtractor mae = new MockArtifactExtractor();
        final ActorRef artifactExtraction = system.actorOf(ArtifactExtraction.props(ImmutableList.of(mae), testProbe.getRef()));
        ImmutableEmail payload = new ImmutableEmail.Builder().build();
        artifactExtraction.tell(payload, ActorRef.noSender());
        ImmutableEmail email = testProbe.expectMsgClass(ImmutableEmail.class);
        assertEquals(ImmutableMap.of("boom", 90), email.getArtifacts());
    }
}