package com.emailparser.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.emailparser.parsers.FeatureExtractor;
import com.google.common.collect.ImmutableMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Extract all the features asynchronously.
 */
public class FeatureExtraction extends AbstractActor {
    private final List<ActorRef> secondaryFeatureExtractions;
    private final ActorRef persistEmailFeature;
    private final List<ActorRef> asyncExtractors;
    final private Map<ImmutableEmail, List<Map<String, Object>>> state = new HashMap<>();

    static class AsyncFeatureExtrationOutput {
        final private ImmutableEmail email;
        final private Map<String, Object> features;

        AsyncFeatureExtrationOutput(ImmutableEmail email, Map<String, Object> features) {
            this.email = email;
            this.features = features;
        }

        public ImmutableEmail getEmail() {
            return email;
        }

        public Map<String, Object> getFeatures() {
            return features;
        }
    }

    static class AsyncFeatureExtration extends AbstractActor{

        final private FeatureExtractor extractor;
        final private ActorRef featureExtraction;

        public AsyncFeatureExtration(FeatureExtractor extractor, ActorRef featureExtraction) {
            this.extractor = extractor;
            this.featureExtraction = featureExtraction;
        }

        static public Props props(FeatureExtractor extractor, ActorRef featureExtraction) {
            return Props.create(AsyncFeatureExtration.class, () -> {
                return new AsyncFeatureExtration(extractor, featureExtraction);
            });
        }

        public void extractFeatures(ImmutableEmail email){
            Map<String, Object> features = extractor.extract(email);
            AsyncFeatureExtrationOutput o = new AsyncFeatureExtrationOutput(email, features);
            featureExtraction.tell(o, getSelf());
        }

        @Override
        public Receive createReceive() {
            return receiveBuilder()
                    .match(ImmutableEmail.class, this::extractFeatures)
                    .build();
        }
    }

    public FeatureExtraction(List<FeatureExtractor> extractors, ActorRef persistEmailFeature, List<ActorRef> secondaryFeatureExtractions) {
        this.persistEmailFeature = persistEmailFeature;
        this.secondaryFeatureExtractions = secondaryFeatureExtractions;
        this.asyncExtractors = extractors.stream()
                .map((e) -> getContext().actorOf(AsyncFeatureExtration.props(e, getSelf())))
                .collect(Collectors.toList());
    }

    static public Props props(List<FeatureExtractor> extractors, ActorRef persistEmailFeature, List<ActorRef> secondaryFeatureExtractions) {
        return Props.create(FeatureExtraction.class, () -> {
            return new FeatureExtraction(extractors, persistEmailFeature, secondaryFeatureExtractions);
        });
    }

    private void tellToAllSecondaryFeatureExtraction(ImmutableEmail email) {
        for(ActorRef secondaryFeatureExtraction: secondaryFeatureExtractions){
            secondaryFeatureExtraction.tell(email, getSelf());
        }
    }

    private List<ImmutableEmail> getFinishedEmails(){
        List<ImmutableEmail> finishedEmails = new ArrayList<>();
        for(ImmutableEmail email: state.keySet()){
            List<Map<String, Object>> allFeatures = state.get(email);
            if(allFeatures.size() == asyncExtractors.size()){
                finishedEmails.add(email);
            }
        }
        return finishedEmails;
    }

    private void finishEmailsWithAllFeatures() {
        List<ImmutableEmail> finishedEmails = getFinishedEmails();
        communicateResults(finishedEmails);
        clearState(finishedEmails);
    }

    private void clearState(List<ImmutableEmail> finishedEmails) {
        for(ImmutableEmail email: finishedEmails){
            state.remove(email);
        }
    }

    private void communicateResults(List<ImmutableEmail> finishedEmails) {
        for(ImmutableEmail email: finishedEmails){
            ImmutableMap.Builder<String, Object> f = new ImmutableMap.Builder();
            for(Map<String, Object> features: state.get(email)){
                f.putAll(features);
            }
            ImmutableEmail finishedEmail = email.addFeatures(f.build());
            persistEmailFeature.tell(finishedEmail, getSelf());
            tellToAllSecondaryFeatureExtraction(finishedEmail);
        }
    }

    private void addNewFeatureToState(ImmutableEmail email, Map<String, Object> features) {
        if(!state.containsKey(email)){
            state.put(email, new ArrayList<>());
        }
        state.get(email).add(features);
    }

    private void processEmailMessage(ImmutableEmail email){
        for(ActorRef asyncExtractor: asyncExtractors){
            asyncExtractor.tell(email, getSelf());
        }
    }

    private void processNewFeatureMessage(AsyncFeatureExtrationOutput p) {
        addNewFeatureToState(p.getEmail(), p.getFeatures());
        finishEmailsWithAllFeatures();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ImmutableEmail.class, this::processEmailMessage)
                .match(AsyncFeatureExtrationOutput.class, this::processNewFeatureMessage)
                .build();
    }

}
