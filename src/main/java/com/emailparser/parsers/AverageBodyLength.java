package com.emailparser.parsers;

import com.emailparser.actor.ImmutableEmail;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class AverageBodyLength implements SecondaryFeatureExtractor{


    private class AverageExhaustiveStatistic {
        private final int count;
        private final double sum;

        public AverageExhaustiveStatistic(int count, double sum) {
            this.count = count;
            this.sum = sum;
        }

        public AverageExhaustiveStatistic() {
            this.count = 0;
            this.sum = 0;
        }

        public AverageExhaustiveStatistic add(double x){
            return new AverageExhaustiveStatistic(count + 1, sum + x);
        }

        public double average(){
            return sum / count;
        }

    }

    private AverageExhaustiveStatistic state;

    @Override
    public void initialState() {
        state = new AverageExhaustiveStatistic();
    }

    @Override
    public Map<String, Object> computeNewState(ImmutableEmail email) {
        state = state.add(email.getBody().length());
        Map<String, Object> feature = ImmutableMap.<String, Object>of("AverageBodyLength", state.average());
        return feature;
    }
}
