package com.emailparser;

import org.junit.Test;

import static org.junit.Assert.*;

public class utilTest {

    @Test
    public void getEmailCanonicalName() {
        String expt = "0256cd56c95951ed";
        assertEquals(expt, util.getEmailCanonicalName("hello world"));

        expt = "7723c8009c2211b7";
        assertEquals(expt, util.getEmailCanonicalName("hello world2"));
    }
}