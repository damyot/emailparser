# EmailParser

A simple email parser build on top of [akka](https://akka.io)


## Run

The main starts a web application serving a RESTful api.  By default, the
server runs on `0.0.0.0:8080`.

### entry point

The main entry point is 

### Api

| endpoint       |  Method | action          |
|:---------------|:--------|-----------------|
| `/`            | `GET`   | Welcome page    |
| `/emails`      | `GET`   | list all the email ids (todo) |
| `/emails`      | `POST`  | add an email to the db        |
| `/emails/<id>` | `GET`   | get the parsed email from the db (todo) | 

## Hack

### Technologies

EmailParser is a basic maven project.  The main dependency is [akka](https://akka.io).



### Test

Nothing is like good old junit.