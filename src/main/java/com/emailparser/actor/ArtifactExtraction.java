package com.emailparser.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.emailparser.parsers.ArtifactExtractor;
import com.google.common.collect.ImmutableMap;

import java.util.List;

public class ArtifactExtraction extends AbstractActor {
    final List<ArtifactExtractor> extractors;
    private final ActorRef nextStep;

    public ArtifactExtraction(List<ArtifactExtractor> extractors, ActorRef nextStep) {
        this.extractors = extractors;
        this.nextStep = nextStep;
    }

    static public Props props(List<ArtifactExtractor> extractors, ActorRef nextStep) {
        return Props.create(ArtifactExtraction.class, () -> new ArtifactExtraction(extractors, nextStep));
    }

    ImmutableEmail extractArtifacts(ImmutableEmail email){
        ImmutableMap.Builder<String, Object> artifacts = new ImmutableMap.Builder<>();
        for(ArtifactExtractor extractor: extractors){
            artifacts.putAll(extractor.extract(email));
        }
        return email.addArtifacts(artifacts.build());
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(ImmutableEmail.class, email -> {
                    ImmutableEmail emailWithArtifacts = extractArtifacts(email);
                    nextStep.tell(emailWithArtifacts, getSelf());
                })
                .build();
    }
}
