package com.emailparser;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class util {

    public static String toStringSafe(Object o){
        return o == null ? "" : o.toString();
    }

    public static List<String> toListOfStringSafe(List<Object> l){
        return l.stream().map(util::toStringSafe).collect(Collectors.toList());
    }

    public static List<String> toListOfStringSafe(Object... l){
        if(l == null){
            return ImmutableList.of();
        }
        return toListOfStringSafe(Arrays.asList(l));
    }

    /**
     * Returns a predictable and reproducible canonical name of a email.  Under the hood,
     * the function uses siphash and returns a 64 bit hash which is probably too small
     * for production but good enough for a proof of concept.  Anyway siphash is too
     * cool to use something else.
     *
     * @param email the full content of an email
     * @return a UUID like name
     */
    public static String getEmailCanonicalName(String email) {
        final HashFunction hf = Hashing.sipHash24();
        final Hasher hasher = hf.newHasher();
        hasher.putString(email, StandardCharsets.UTF_8);
        return hasher.hash().toString();
    }
}
