package com.emailparser;

import akka.Done;
import akka.actor.ActorSystem;
import akka.http.javadsl.server.AllDirectives;
import static akka.http.javadsl.server.PathMatchers.longSegment;
import akka.http.javadsl.server.Route;
import akka.http.javadsl.unmarshalling.Unmarshaller;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;


public class WebApp extends AllDirectives {

    private final Persistor persistor;
    private final EmailPipeline emailPipeline;


    public WebApp(final ActorSystem system) {
        persistor = new EmailPersistor();
        emailPipeline = new ActorBasedEmailPipeline(system);
    }

    private String wrapBody(String body){
        return String.format("<!doctype html>\n" +
                "<html>\n" +
                "    <head>\n" +
                "        <meta charset=\"utf-8\" />\n" +
                "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n" +
                "        <title>Email Parser</title>\n" +
                "    </head>\n" +
                "    <body>\n" +
                "        %s" +
                "    </body>\n" +
                "</html>", body);
    }

    Route index() {
        return path("", () ->
                get(() ->
                        complete("<h1>index.html</h1>")));
    }

    Route getEmail() {
        final String body = "TODO: retreive email with id %s";
        return pathPrefix("emails", () ->
                path(longSegment(), (Long id) -> complete(wrapBody(String.format(body, id)))));
    }

    Route getEmails(){
        final String body = "TODO: list all available email ids.";
        return path("emails", () ->
                get(() ->
                        complete(wrapBody(body))));
    }

    private CompletionStage<Done> persistAndPassToPipeline(String email) {
        String canonicalName = util.getEmailCanonicalName(email);
        persistor.persist(canonicalName, email);
        emailPipeline.process(canonicalName, email);
        return CompletableFuture.completedFuture(Done.getInstance());
    }

    Route newEmail(){
        return post(() ->
                path("emails", () ->
                        entity(Unmarshaller.entityToString(), order -> {
                            persistAndPassToPipeline(order);
                            return complete(wrapBody("success"));
                        })));
    }

    Route emails(){
        return route(getEmail(), getEmails(), newEmail());
    }

    public Route createRoute() {
        return route(index(), emails());
    }
}
