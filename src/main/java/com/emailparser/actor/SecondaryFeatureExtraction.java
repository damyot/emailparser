package com.emailparser.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.emailparser.parsers.SecondaryFeatureExtractor;

public class SecondaryFeatureExtraction extends AbstractActor {

    final SecondaryFeatureExtractor extractor;
    private final ActorRef secondaryFeaturePersistor;

    public SecondaryFeatureExtraction(SecondaryFeatureExtractor extractor, ActorRef secondaryFeaturePersistor) {
        this.extractor = extractor;
        this.secondaryFeaturePersistor = secondaryFeaturePersistor;
    }

    static public Props props(SecondaryFeatureExtractor extractor, ActorRef nextStep) {
        return Props.create(SecondaryFeatureExtraction.class, () -> {
            extractor.initialState();
            return new SecondaryFeatureExtraction(extractor, nextStep);
        });
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(ImmutableEmail.class, email -> {
                    // TODO: wrap features in email.
                    secondaryFeaturePersistor.tell(extractor.computeNewState(email), getSelf());
                })
                .build();
    }

}
