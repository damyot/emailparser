package com.emailparser.parsers;

import com.emailparser.actor.ImmutableEmail;

import java.util.Map;

/**
 * The idea behing SecondaryFeatureExtractor is the reduce function.
 *
 * The initialState method is used to compute the initial state.  You are free to do
 * what ever you want in that function (service call, computation,...) to set your internal
 * state.  This can be useful when recovering from a crash.
 *
 * Following the execution of the initial state, every email will pass by the computeNewState
 * function.  You should do two things in this function.
 *
 *   1. update you internal state
 *   2. publish features that will be else where.
 *
 */
public interface SecondaryFeatureExtractor {

    void initialState();

    Map<String, Object> computeNewState(ImmutableEmail email);
}
