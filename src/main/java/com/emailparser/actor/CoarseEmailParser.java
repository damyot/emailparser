package com.emailparser.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.emailparser.util;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CoarseEmailParser extends AbstractActor {

    static public Props props(ActorRef nextStep) {
        return Props.create(CoarseEmailParser.class, () -> new CoarseEmailParser(nextStep));
    }

    static public class RawEmail {
        final String raw;
        final String canonicalName;

        public RawEmail(String raw, String canonicalName) {
            this.raw = raw;
            this.canonicalName = canonicalName;
        }
    }

    private final ActorRef nextStep;

    public CoarseEmailParser(ActorRef nextStep) {
        this.nextStep = nextStep;
    }

    public ImmutableEmail parseEmail(RawEmail email) {

        Session s = Session.getInstance(new Properties());
        InputStream is = new ByteArrayInputStream(email.raw.getBytes());
        try {
            MimeMessage message = new MimeMessage(s, is);
            ImmutableEmail.Builder builder = new ImmutableEmail.Builder();
            Address[] to = message.getRecipients(Message.RecipientType.TO);
            Address[] cc = message.getRecipients(Message.RecipientType.CC);
            Address[] bcc = message.getRecipients(Message.RecipientType.BCC);
            builder.subject(message.getSubject())
                    .canonicalName(email.canonicalName)
                    .sender(util.toStringSafe(message.getSender()))
                    .body(util.toStringSafe(message.getContent()))
                    .cc(util.toListOfStringSafe(cc))
                    .bcc(util.toListOfStringSafe(bcc))
                    .to(util.toListOfStringSafe(to));
            return builder.build();
        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(RawEmail.class, rawEmail -> {
                    nextStep.tell(parseEmail(rawEmail), getSelf());
                })
                .build();
    }
}
