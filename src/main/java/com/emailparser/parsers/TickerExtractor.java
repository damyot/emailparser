package com.emailparser.parsers;

import com.emailparser.actor.ImmutableEmail;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;


import java.util.List;

public class TickerExtractor implements ArtifactExtractor{
    private final List<String> tickers = ImmutableList.of("goog", "msft", "ge");

    @Override
    public ImmutableMap<String, Object> extract(ImmutableEmail email) {
        ImmutableMap.Builder<String, Object> ret = new ImmutableMap.Builder<>();
        for(String ticker: tickers){
            int idx = email.getBody().indexOf(ticker);
            if(idx >= 0){
                ret.put("ticker:" + ticker, idx);
            }
        }
        return ret.build();
    }
}
