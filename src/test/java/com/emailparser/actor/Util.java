package com.emailparser.actor;

import com.google.common.io.Resources;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Util {

    public static CoarseEmailParser.RawEmail getEmail(String fixtureName) throws IOException {
        ClassLoader classLoader = Util.class.getClassLoader();
        String rawEmail = Resources.toString(Resources.getResource(fixtureName), StandardCharsets.UTF_8);
        return new CoarseEmailParser.RawEmail(rawEmail, "email_01.msg");
    }
}
