package com.emailparser.actor;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ImmutableEmail {
    final private String canonicalName;
    final private String body;
    final private String sender;
    final private String subject;
    final private List<String> cc;
    final private List<String> to;
    final private List<String> bcc;

    final private Map<String, Object> artifacts;
    final private Map<String, Object> features;



    public static class Builder{
        private String canonicalName;
        private String body;
        private String sender;
        private String subject;
        private List<String> cc;
        private List<String> bcc;
        private List<String> to;
        private Map<String, Object> artifacts;
        private Map<String, Object> features;

        public Builder canonicalName(String canonicalName){
            this.canonicalName = canonicalName;
            return this;
        }

        public Builder body(String body){
            this.body = body;
            return this;
        }

        public Builder sender(String sender) {
            this.sender = sender;
            return this;
        }

        public Builder subject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder cc(List<String> cc) {
            this.cc = ImmutableList.copyOf(cc);
            return this;
        }

        public Builder to(List<String> to) {
            this.to = to;
            return this;
        }

        public Builder bcc(List<String> bcc) {
            this.bcc = bcc;
            return this;
        }

        public Builder artifacts(Map<String, Object> artifacts) {
            this.artifacts = ImmutableMap.copyOf(artifacts);
            return this;
        }

        public Builder features(Map<String, Object> features) {
            this.features = ImmutableMap.copyOf(features);
            return this;
        }

        public ImmutableEmail build(){
            return new ImmutableEmail(
                    this.canonicalName == null ? "" : this.canonicalName,
                    this.body == null ? "" : this.body,
                    this.sender == null ? "" : this.sender,
                    this.subject == null ? "" : this.subject,
                    this.cc ==  null ? ImmutableList.of() : this.cc,
                    this.to ==  null ? ImmutableList.of() : this.to,
                    this.bcc ==  null ? ImmutableList.of() : this.bcc,
                    this.artifacts == null ?ImmutableMap.of(): this.artifacts,
                    this.features == null ? ImmutableMap.of(): this.features
            );
        }
    }

    public ImmutableEmail(String canonicalName, String body, String sender, String subject, List<String> cc, List<String> to, List<String> bcc, Map<String, Object> artifacts, Map<String, Object> features) {
        this.canonicalName = canonicalName;
        this.body = body;
        this.sender = sender;
        this.subject = subject;
        this.cc = cc;
        this.to = to;
        this.bcc = bcc;
        this.artifacts = artifacts;
        this.features = features;
    }

    public ImmutableEmail addArtifacts(ImmutableMap<String, Object> artifacts) {
        ImmutableMap.Builder<String, Object> newArtifacts = new ImmutableMap.Builder();
        newArtifacts.putAll(artifacts);
        newArtifacts.putAll(getArtifacts());

        return new Builder()
                .canonicalName(canonicalName)
                .sender(sender)
                .cc(cc)
                .bcc(bcc)
                .body(body)
                .subject(subject)
                .to(to)
                .artifacts(newArtifacts.build())
                .build();
    }

    public ImmutableEmail addFeatures(ImmutableMap<String, Object> features) {
        ImmutableMap.Builder<String, Object> newFeatures = new ImmutableMap.Builder();
        newFeatures.putAll(features);
        newFeatures.putAll(getFeatures());

        return new Builder()
                .canonicalName(canonicalName)
                .sender(sender)
                .cc(cc)
                .bcc(bcc)
                .body(body)
                .subject(subject)
                .to(to)
                .artifacts(artifacts)
                .features(newFeatures.build())
                .build();
    }

    public String getBody() {
        return body;
    }

    public String getSender() {
        return sender;
    }

    public String getSubject() {
        return subject;
    }

    public List<String> getCc() {
        return cc;
    }

    public List<String> getTo() {
        return to;
    }

    public List<String> getBcc() {
        return bcc;
    }

    public Map<String, Object> getArtifacts() {
        return artifacts;
    }

    public Map<String, Object> getFeatures() {
        return features;
    }

    public String getCanonicalName() {
        return canonicalName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImmutableEmail that = (ImmutableEmail) o;
        return Objects.equals(canonicalName, that.canonicalName) &&
                Objects.equals(body, that.body) &&
                Objects.equals(sender, that.sender) &&
                Objects.equals(subject, that.subject) &&
                Objects.equals(cc, that.cc) &&
                Objects.equals(to, that.to) &&
                Objects.equals(bcc, that.bcc) &&
                Objects.equals(artifacts, that.artifacts) &&
                Objects.equals(features, that.features);
    }

    @Override
    public int hashCode() {
        return Objects.hash(canonicalName, body, sender, subject, cc, to, bcc, artifacts, features);
    }

    @Override
    public String toString() {
        return "ImmutableEmail{" +
                "canonicalName='" + canonicalName + '\'' +
                ", body='" + body + '\'' +
                ", sender='" + sender + '\'' +
                ", subject='" + subject + '\'' +
                ", cc=" + cc +
                ", to=" + to +
                ", bcc=" + bcc +
                ", artifacts=" + artifacts +
                ", features=" + features +
                '}';
    }
}
