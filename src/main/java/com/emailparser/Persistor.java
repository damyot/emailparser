package com.emailparser;

public interface Persistor {

    void persist(String canonicalName, String email);
}
