package com.emailparser.actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import com.emailparser.parsers.ArtifactExtractor;
import com.emailparser.parsers.FeatureExtractor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class FeatureExtractionTest {

    ActorSystem system;

    @Before
    public void setup() {
        system = ActorSystem.create();
    }

    @After
    public void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    static class MockFeatureExtractor implements FeatureExtractor {

        @Override
        public ImmutableMap<String, Object> extract(ImmutableEmail email) {
            return ImmutableMap.of("boom", 90);
        }
    }

    @Test
    public void createReceive() throws IOException {
        TestKit testProbe1 = new TestKit(system);
        TestKit testProbe2 = new TestKit(system);
        List<ActorRef> secondaryFeature = ImmutableList.of(testProbe2.getRef());

        MockFeatureExtractor mae = new MockFeatureExtractor();
        final ActorRef artifactExtraction = system.actorOf(FeatureExtraction.props(ImmutableList.of(mae), testProbe1.getRef(), secondaryFeature));
        ImmutableEmail payload = new ImmutableEmail.Builder().build();
        artifactExtraction.tell(payload, ActorRef.noSender());

        ImmutableEmail email = testProbe1.expectMsgClass(ImmutableEmail.class);
        assertEquals(ImmutableMap.of("boom", 90), email.getFeatures());

        email = testProbe2.expectMsgClass(ImmutableEmail.class);
        assertEquals(ImmutableMap.of("boom", 90), email.getFeatures());
    }

}