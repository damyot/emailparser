package com.emailparser;

public interface EmailPipeline {

    void process(String canonicalName, String emailFullContent);

}
