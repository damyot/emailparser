package com.emailparser;


import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;

import java.io.IOException;
import java.util.concurrent.CompletionStage;

public class Main  {

    static void shutdownWebServer(CompletionStage<ServerBinding> binding, ActorSystem system) {
        binding
                .thenCompose(ServerBinding::unbind) // trigger unbinding from the port
                .thenAccept(unbound -> system.terminate()); // and shutdown when done
    }

    static void createWebServer(String host, int port) throws IOException {
        ActorSystem system = ActorSystem.create("EmailParser");

        final Http http = Http.get(system);
        final ActorMaterializer materializer = ActorMaterializer.create(system);

        //In order to access all directives we need an instance where the routes are define.
        WebApp app = new WebApp(system);

        final Flow<HttpRequest, HttpResponse, NotUsed> routeFlow = app.createRoute().flow(system, materializer);
        final CompletionStage<ServerBinding> binding = http.bindAndHandle(routeFlow,
                ConnectHttp.toHost(host, port), materializer);

        String msg = String.format("Server online at http://%s:%s/\nPress RETURN to stop...", host, port);
        System.out.println(msg);
        System.in.read(); // let it run until user presses return
        shutdownWebServer(binding, system);
    }

    public static void main(String[] args) throws Exception {
        createWebServer("0.0.0.0", 8080);
    }

}