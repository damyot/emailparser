package com.emailparser.parsers;

import com.emailparser.actor.ImmutableEmail;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class TickerExtractorTest {

    @Test
    public void extract() {
        TickerExtractor extractor = new TickerExtractor();
        String body = "foo goog toto msft";
        ImmutableEmail email = new ImmutableEmail.Builder().body(body).build();
        Map<String, Object> expt = ImmutableMap.of("ticker:goog", 4,
                                                   "ticker:msft", 14);
        assertEquals(expt, extractor.extract(email));
    }
}