package com.emailparser.parsers;

import com.emailparser.actor.ImmutableEmail;
import com.google.common.collect.ImmutableMap;


public interface ArtifactExtractor {

    /**
     * Extract an artifact from the email.  Please return something immutable.
     * You have access to all the email but the "artifact" section is empty.
     * @param email,
     * @return a feature
     */
    ImmutableMap<String, Object> extract(ImmutableEmail email);
}
