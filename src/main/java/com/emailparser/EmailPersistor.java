package com.emailparser;

import com.google.cloud.storage.*;

import java.nio.charset.StandardCharsets;

public class EmailPersistor implements Persistor {

    /**
     * Persist the email with the canonicalName in the cloud.  This method blocks
     * to make sure email is saved when the user receive the 201 status.
     * @param canonicalName
     * @param email
     */
    @Override
    public void persist(String canonicalName, String email) {
        Storage storage = StorageOptions.getDefaultInstance().getService();
        BlobId blobId = BlobId.of("emailparser", canonicalName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("text/plain").build();
        Blob blob = storage.create(blobInfo, email.getBytes(StandardCharsets.UTF_8));
    }
}
