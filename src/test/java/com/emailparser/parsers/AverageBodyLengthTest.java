package com.emailparser.parsers;

import com.emailparser.actor.ImmutableEmail;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class AverageBodyLengthTest {

    @Test
    public void reduceLogic() {
        AverageBodyLength avg = new AverageBodyLength();
        avg.initialState();
        ImmutableEmail email = new ImmutableEmail.Builder().body("one").build();
        Map<String, Object> features = avg.computeNewState(email);
        assertEquals(ImmutableMap.of("AverageBodyLength", 3.0), features);

        email = new ImmutableEmail.Builder().body("four").build();
        features = avg.computeNewState(email);
        assertEquals(ImmutableMap.of("AverageBodyLength", (3.0 + 4.0) / 2.0), features);

    }
}