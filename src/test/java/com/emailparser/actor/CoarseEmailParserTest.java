package com.emailparser.actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class CoarseEmailParserTest {
    ActorSystem system;

    @Before
    public void setup() {
        system = ActorSystem.create();
    }

    @After
    public void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }



    @Test
    public void createReceive() throws IOException {
        final TestKit testProbe = new TestKit(system);
        final ActorRef coarseEmailParser = system.actorOf(CoarseEmailParser.props(testProbe.getRef()));

        coarseEmailParser.tell(Util.getEmail("email_01.msg"), ActorRef.noSender());
        ImmutableEmail email = testProbe.expectMsgClass(ImmutableEmail.class);
        assertEquals(ImmutableList.of(), email.getBcc());
        List<String> expt = ImmutableList.of("chelsea.bardal@enron.com","jason.biever@enron.com", "stephane.brodeur@enron.com");
        assertEquals(expt, email.getTo());
    }

    @Test
    public void createReceiveEmptyMessage() throws IOException {
        final TestKit testProbe = new TestKit(system);
        final ActorRef helloGreeter = system.actorOf(CoarseEmailParser.props(testProbe.getRef()));

        helloGreeter.tell(Util.getEmail("email_02.msg"), ActorRef.noSender());
        ImmutableEmail email = testProbe.expectMsgClass(ImmutableEmail.class);
        assertEquals(ImmutableList.of(), email.getBcc());
        assertEquals(ImmutableList.of(), email.getTo());
    }
}